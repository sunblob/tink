import { downloadFile } from './utils/download';
import { parseCsv } from './utils/parse-csv';
import { unzipFile } from './utils/unzip';

async function main() {
  await downloadFile('https://invest-public-api.tinkoff.ru/history-data?figi=BBG004S68507&year=2022', 'kek.zip');

  await unzipFile('kek.zip');

  const res = parseCsv('files/BBG004S68507_20220103.csv');

  // console.log(res);
}

main();
