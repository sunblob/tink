import fs from 'fs';

export function parseCsv(filePath: string) {
  const startTime = performance.now();
  let data = fs.readFileSync(filePath, 'utf8');

  const res = data.split('\n').map((record) => {
    const recordArr = record.split(';');

    return {
      figi: recordArr[0],
      utc: recordArr[1],
      open: recordArr[2],
      close: recordArr[3],
      low: recordArr[4],
      high: recordArr[5],
      volume: recordArr[6],
    };
  });

  const endTime = performance.now();

  console.log(`PARSING TOOK TIME: ${endTime - startTime} milliseconds`);

  return res;
}
