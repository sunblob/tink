import ezip from 'extract-zip';
import path from 'path';

export async function unzipFile(filePath: string) {
  try {
    await ezip(filePath, { dir: path.join(__dirname, '..', '..', 'files') });
    console.log('finished unzipping');
  } catch (error) {
    console.log('error while unzipping', error);
  }
}
