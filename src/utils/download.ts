import * as stream from 'stream';
import { promisify } from 'util';
import fs from 'fs';
import axios from 'axios';
import config from '../../config.json';

const finished = promisify(stream.finished);

export async function downloadFile(fileUrl: string, outputLocationPath: string) {
  const writer = fs.createWriteStream(outputLocationPath);
  return axios
    .get(fileUrl, {
      headers: {
        Authorization: `Bearer ${config.token}`,
      },
      responseType: 'stream',
    })
    .then((response) => {
      response.data.pipe(writer);
      return finished(writer); //this is a Promise
    });
}

// curl --request GET \
//   --url 'https://invest-public-api.tinkoff.ru/history-data?figi=BBG004S68507&year=2022' \
//   --header 'Authorization: Bearer t.9zL9M7bQdNschbLn5EZMAB6bJbY94_WQd_6qG7krXZJrCaQzU2wvqh9zrQYUG5rWm4m67s4sANNjrd4q6U2D4Q' --output kek.zip
